<?php

namespace App\Utils;

use App\Jobs\GenerateCharacterImage;
use App\Jobs\GenerateImage;
use App\Models\Character;
use App\Models\Turn;
use App\Models\World;
use Barryvdh\Debugbar\Facades\Debugbar;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Str;

class StoryGenerator
{
    public $client;

    public function __construct()
    {
        ini_set('max_execution_time', 120);
        $this->client = \OpenAI::client(env('OPENAI_API_KEY'));
    }

    public function generateWorld($world)
    {
        $prompt = view('prompts.world', compact('world'))->render();
//        dump($prompt);
        // measure time to response
        $start = microtime(true);

        $response = $this->client->completions()->create([
            'prompt' => $prompt,
            'model' => 'text-davinci-003',
            'max_tokens' => 500,
            'temperature' => 0.7,
            'top_p' => 1,
            'n' => 1,
            'stream' => false,
            'logprobs' => null,
            'frequency_penalty' => .17,

        ]);
        $end = microtime(true);
        $time = $end - $start;

        $generation = $response['choices'][0]['text'];

        // now lets try to preg_match the generated text to extract the responses from this template:
        /*
World name: World Name
Starting city: City Name
World Setting: A paragraph or two describing this world, its inhabitants, and the background for our story
Art style: A list of 5-6 adjectives describing this world for our illustrations of backdrops, separated by commas
Character Art Style: A list of 5-6 adjectives describing characters for portraits, separated by commas
Your quest: A paragraph or two explaining why must our adventurers travel and explore this world? What is their goal?
Goal name: The name of the goal
Goal destination: The place the goal is found
Goal reason: The reason for the goal (eg fame and fortune)
Route: List the key places they will pass on their journey with a short description for each.
Challenges: The enemies, creatures dangers and scenarios they will encounter, as well as any hazards along the way

         * */

        preg_match_all("/(.*):\s*\n?\s*(.*)/i", "World Name: " . $generation, $matches);
        // store each of the matches in the world object using snake case properties
        foreach ($matches[1] as $key => $match) {
            $world->setAttribute(Str::snake($match), $matches[2][$key]);
        }


        // save the world
//        $world->save();
        // log the usage of our API credits
        $this->logUsage('generateWorld', $response->usage, $time);
        // return the world
        return $world;


    }

    public function generateCharacter(World $world)
    {
        $prompt = view('prompts.chat-prompts.character-minor-race', compact('world'))->render();
//        dump($prompt);
        // measure time to response
        $start = microtime(true);

        $response = $this->client->chat()->create([
            'messages' => [
                [
                    'role' => 'system',
                    'content' => $prompt
                ],
                // 10 new characters could be 'male fighters' or 'Irish rogues' or whatever
                [
                    'role' => 'user',
                    'content' => 'Generate 8 new characters. Do not numerate them or use indents. Avoid cliched or stereotypical illustrations. Use the whole template including their level, stats and backstory. Separate each character with a double line break'
                ],
            ],
            'model' => 'gpt-3.5-turbo',
            'max_tokens' => 2000,
            'temperature' => 0.7,
            'top_p' => 1,
            'n' => 1,
            'stream' => false,

        ]);
        $end = microtime(true);
        $time = $end - $start;

        $generation = $response['choices'][0]['message']['content'];

        $chars = preg_split("/\n\n/i", $generation);

        // now lets try to preg_match the generated text to extract the responses from this template:
        dump($chars);
        foreach ($chars as $char) {

            preg_match_all("/(.*):\s*\n?\s*(.*)/i", $char, $matches);
            // store each of the matches in the world object using snake case properties

            $attrs = [];
            foreach ($matches[1] as $key => $match) {
                $attrs[Str::snake(strtolower(trim($match)))] = $matches[2][$key];
            }
            dump($attrs);
            $character = new Character([
                'world_id' => $world->id,
                'attributes' => $attrs
            ]);

            // handle stats
            $stats = $attrs['stats'];
            preg_match_all("/(\w+)\s*:?\s*(\d+)/i", $stats, $matches);
            $stats = [
                'hp' => 100,
                'xp' => 0,
                'satiety' => 100,
            ];
            foreach ($matches[1] as $key => $match) {
                $stats[strtolower($match)] = $matches[2][$key];
            }
            $attrs['stats'] = $stats;
            $character->attributes = $attrs;
            $missing = false;
            foreach(['name','inventory','backstory','weapon'] as $key){
                if(!isset($attrs[$key])){
                    dump('missing '.$key);
                    $missing = true;
                }
            }

            dump($character);


            if($missing) continue;
            $character->save();
            GenerateCharacterImage::dispatch($character);
//        Cache::tags([request()->ip()])->put('character', $character);


        }
//        GenerateImage::dispatchNow('character');
        $this->logUsage('generateCharacter', $response->usage, $time);
        return $character;

    }

    public function generateTurn(World $world, Character $character, $history, $action)
    {
        $lastTurn = Cache::tags([request()->ip()])->get('last_turn');
        $prompt = view('prompts.turn-condensed', compact('world', 'character', 'history', 'action', 'lastTurn'))->render();
//        dump($prompt);
        // measure time to response
        $start = microtime(true);

        $response = $this->client->completions()->create([
            'prompt' => $prompt,
            'model' => 'text-davinci-003',
            'max_tokens' => 500,
            'temperature' => 0.8,
            'top_p' => 1,
            'n' => 1,
            'stream' => false,
            'logprobs' => null,
            'frequency_penalty' => .17,

        ]);
        $end = microtime(true);
        $time = $end - $start;


        $generation = $response['choices'][0]['text'];
//        dump($response['choices']);

        // now lets try to preg_match the generated text to extract the responses from this template:
        /*
         * Action taken: The last thing you did
Dice roll: The result of the dice roll
Next Turn: A detailed paragraph describing your location in rich detail, and any characters, challenges or encounters you face.
Illustration: A sentence describing the scene for our illustrator, with a list of descriptive adjectives, separated by commas
NPC Illustration: A sentence describing the NPC for our illustrator, with a list of descriptive adjectives, separated by commas, or none if there is no NPC
NPC Weapon: The name of the NPC's weapon, or none if they are unarmed or if there is no NPC
NPC Stats: The NPC's stats, or none
NPC Level: The NPC's level, or none
NPC Damage: The NPC's damage, or none
Actions: A list of 3 actions you could take, for example:
- Attack the NPC
- Sneak past the NPC
- Talk to the NPC
        */

//        preg_match_all("/(^[^:]*):\s*\n?\s*(.*)/m", $generation, $matches);
        preg_match_all("/(^[^:]*):(.+)$/m", $generation, $matches);

        // store each of the matches in the turn object using snake case properties
        $turn = new \stdClass();
        foreach ($matches[1] as $key => $match) {

            $turn->{Str::snake($match)} = $matches[2][$key];
        }

        // santity check the turn - if we have no actions our generation has failed
        foreach (['stats', 'inventory', 'weapon', 'buffs', 'location', 'h_p', 'x_p', 'n_p_cs', 'actions', 'party'] as $prop) {
            if (!isset($turn->{$prop})) {
//                dump($generation, $turn, $matches);
//                dd('Couldnt find ' . $prop);
//                return false;
            }
        }

        // if we have a new party member, ensure they are no longer in the npc list
        /*if ($turn->party) {
            $npcs = explode(',', $turn->n_p_cs);
            $npcs = array_filter($npcs, function ($npc) use ($turn) {
                return trim(strtolower($npc)) != trim(strtolower($turn->party));
            });
            $turn->n_p_cs = implode(',', $npcs);
        }*/

        // update the history
        $history[] = "You " . $action . " (Roll {$turn->dice_roll})";  // - seems to make a loop?
        $history[] = $turn->summary ?? '';

        Cache::tags([request()->ip()])->put('history', $history);
//        dump($turn);
        // update the character
        foreach (['stats', 'inventory', 'weapon', 'buffs', 'location', 'h_p', 'x_p', 'n_p_cs', 'party'] as $attribute) {
            // todo - if new attribute is empty/missing, use the old value

            $character->{$attribute} = trim($turn->{$attribute} ?? $lastTurn->{$attribute} ?? '');
        }
        Cache::tags([request()->ip()])->put('character', $character);

        $turn->prompt = $prompt;
        $turn->generation = $generation;
        $turn->last_action = $action;
        Cache::tags([request()->ip()])->put('last_turn', $turn);
        if (!empty($turn->n_p_cs)) {
            // only generate an image if the NPC has changed since last turn
            if (($lastTurn->n_p_cs ?? '') != $turn->n_p_cs) {
//                GenerateImage::dispatchNow('turn');
            }
        }
//        GenerateImage::dispatchNow('background');
        $this->logUsage('generateTurn', $response->usage, $time);
        return $turn;

    }


    // this function is used to log the usage of our API credits
    public function logUsage($function, $usage, $time)
    {
        /*   $log = new \App\Models\OpenAIUsageLog();
           $log->function = $function;
           $log->usage = $usage;
           $log->save();*/
        $cost = $usage->totalTokens / 1000 * 0.02;
//        dump($function, $usage, '$' . $cost, $time);
    }

    public function getHistory($character, $limit=30)
    {
        // todo: pull from turn database
        $historic = Turn::where('character_id', $character->id)->orderBy('created_at', 'desc')->limit($limit)->get();
        return $historic;
    }

    public function getCharacterAsJson($character)
    {
//        $character = Cache::tags([request()->ip()])->get('character', new Character());
        /* USing this type
        const player: Player = {
    description: {illustration: 'dark handsome warrior with scar on left cheek', name: 'Bambista'},
    status: {HP: 100, XP: 0, Level: 1, Strength: 18, Dexterity: 7, Constitution: 14, Intelligence: 4},
    inventory: ['sword', 'shield', 'magical amulet'],
    weapon: 'sword',
    party: []
};
        */
        return [
            'description' => [
                'illustration' => $character->attributes['illustration_description'],
                'name' => $character->attributes['name'],
            ],
            'status' => [
                'hp' => $character->attributes['stats']['hp'] ?? '',
                'xp' => $character->attributes['stats']['xp'] ?? '',
                'level' => $character->attributes['stats']['level'] ?? '',
                'strength' => $character->attributes['stats']['strength'] ?? '',
                'agility' => $character->attributes['stats']['agility'] ?? '',
                'stealth' => $character->attributes['stats']['stealth'] ?? '',
                'charisma' => $character->attributes['stats']['charisma'] ?? '',
                'intelligence' => $character->attributes['stats']['intelligence'] ?? '',
            ],
            'inventory' => explode(',', $character->inventory ?? ''),
            'weapon' => $character->attributes['weapon'] ?? 'none',
            'party' =>  $character->attributes['party'] ?? []
        ];

    }

    public function checkOutcome($turn)
    {
        // validate turn object conforms to our expectations
        /*
         * export type Turn = {
    action: string // last action taken
        diceRoll: number//if below 5 outcome is bad
    outcome: string //a detailed paragraph of 4-5 sentences explaining what you just did and what happened next
    itemsFound?: item[] // items discovered
    itemsLost?: item[] // items lost
    location: location // where you are now
    summary: string // summarize the outcome paragraph in two sentences. Include names and places
    playerChange?: Change //optional change to hp, xp, etc since last turn

}
         */
        foreach (['action', 'diceRoll', 'outcome', 'location', 'summary'] as $prop) {
            if (!isset($turn->{$prop})) {
                throw new \Exception('Couldnt find ' . $prop);
//                return false;
            }

        }



    }

    public function minify($prompt)
    {
        // remove any extra whitespace, new lines, tabs etc
        $prompt = preg_replace('/\s+/', ' ', $prompt);
        return $prompt;
    }

    public function typescriptTurn(World $world, Character $character, $history, $action)
    {
        $player = $this->getCharacterAsJson($character);
        $previousTurns = $this->getHistory($character, 30)  ;
        $history = $previousTurns->pluck('turn_response.summary');
//        $world = Cache::tags([request()->ip()])->get('world', new World());
//        $prompt = view('prompts/turn-ts/turn.ts',compact('player','history','world'))->render();
        $start = microtime(true);
        $parameters = [
            'messages' => [
                [
                    'role' => 'system',
                    'content' => str_replace("&quot;",'"',$this->minify(view('prompts.turn-ts.system', compact('player', 'world'))->render()))
                ],
                [
                    'role' => 'user',
                    'content' => '// Sample output of generateTurn("Teleport to a new location", 6, player, history)'
                ],
                [
                    'role' => 'assistant',
                    'content' => $this->minify(view('prompts.turn-ts.assistant', compact('player', 'world'))->render())
                ],
                [
                    'role' => 'user',
                    'content' => $this->minify(view('prompts.turn-ts.player-turn', compact('player', 'history', 'world', 'action'))->render())
                ]
            ],
            'model' => 'gpt-3.5-turbo',
            'max_tokens' => 500,
            'temperature' => 0.8,
            'top_p' => 1,
            'n' => 1,
            'stream' => false,
            'frequency_penalty' => .17,

        ];
        Debugbar::info('Sending request to OpenAI');
        Debugbar::info($parameters);

        $response = $this->client->chat()->create($parameters);
        $end = microtime(true);
        $time = $end - $start;
        Debugbar::info($time . ' seconds to get response');
        Debugbar::info($response);


        $generation = $response['choices'][0]['message']['content'];
        $generation = trim($generation,'`');
        $turn = json_decode($generation);
        if (!is_object($turn)) {
            dd($response);
        }
        Debugbar::info($turn);
        // good place to store prompt + outcome

        // validate response contains all the expected properties
        $this->checkOutcome($turn);

        // if our location hasn't changed, we can just use the previous location image
        /*if ($turn->location->changed == false) {
            if($previousTurns->first()) $locationImage = $previousTurns->first()->location_image;
        }*/
        // should be able to do this with npcs too, but not in our prompt yet

        $turnData = [
            'character_id' => $character->id,
            'world_id' => $world->id,
            // store the history or just rebuild from db and turn index?
            'player_state' => ['player' => $player, 'action' => $action,],
            'dice_roll' => $turn->diceRoll,
            'turn_number' => $turn->turnNumber ?? 0, // wtf was this?
            'turn_response' => $turn,
            'location_image' => $locationImage ?? null,

        ];
//        dump($turn);
//        dump($turnData);
        $turn_model = Turn::create($turnData);



        Debugbar::info($turn_model);
        $attributes = $character->attributes;


        // need to actually test these properties exist before acting on them
        // now update attributes['stats'] hp + xp based on the turn's changes
        $attributes['stats']['hp'] = $attributes['stats']['hp'] + ($turn->playerChange->hp ?? 0);
        $attributes['stats']['xp'] = $attributes['stats']['xp'] + ($turn->playerChange->xp ?? 0);

        if ($attributes['stats']['hp'] <= 0) $attributes['dead'] = true;
        // level up if xp is over 100
        if ($attributes['stats']['xp'] >= $attributes['level'] * 100) {
            $turn->levelled_up = true;
            $attributes['level'] = $attributes['level'] + 1;
            $attributes['stats']['xp'] = 0;
        }



        // is this allowed? should we limit it?
        if (abs($turn->playerChange->level ?? 0) < 2) $attributes['level'] = $attributes['level'] + ($turn->playerChange->level ?? 0);

        // update the character in the db
        $character->update([
            'attributes' => $attributes,
//            'location' => $turn->location,
//            'turn_id' => $turn_model->id,
        ]);


        // return the turn
        return $turn;


    }

    public function gravestone($player, $killed_by)
    {

        $parameters = [
            'messages' => [
                [
                    'content' => 'This is a parody tombstone generator for an RPG game we are playing.\nWhen a user dies, please create a witty inscription for their tombstone. This could be in the form of a ditty, a limerick or others, and might include how they died or the monster that killed them',
                    'role' => 'system'
                ],
                [
                    'content' => $player . ' was killed by ' . $killed_by,
                    'role' => 'user'
                ],

            ],
            'model' => 'gpt-3.5-turbo',
            'max_tokens' => 500,
            'temperature' => 0.8,
            'top_p' => 1,

        ];

        $response = $this->client->chat()->create($parameters);
        $generation = $response['choices'][0]['message']['content'];
        return $generation;
    }

}
