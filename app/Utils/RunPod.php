<?php

namespace App\Utils;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class RunPod
{
    public $baseStyle = 'modelshoot style, professional photograph, two thirds rule, Unreal Engine 4k, intricate details, dreamlikeart, octane render, masterpiece, award winning dslr raw photo, medieval, rpg, headshot, sexy. nsfw, (cleavage:1.1), (ragged, weathered,filthy, dirty, sweaty, worn, ripped, torn clothes:1.6), cinematic lighting, headshot, (moody:0.6) (gritty:0.3)';
    public $negativeStyle = 'obese, extra nipples, body overlay, smoothing, two people, disproportionate lowres, bad anatomy, bad hands, by bad artist, text, error, missing fingers, extra digit, fewer digits, jpeg artifacts, watermark, blurry, artist name, fat, obese, extra fingers, mutated, (disembodied), (poorly drawn), (ugly), blurry, (bad anatomy), (bad proportions), (extra limbs), 3d, (red eyes), (green skin:1.2), cgi, pixar, cartoon, (modern:1.5), (armor:1.5), (nipples:1.6), naked, clean, facial markings, over saturated, face paint, black and white, grayscale, denim, jeans, (big ears:1.2)';

    public $character = [
        'height' => 512,
        'width' => 512,
        'negative_prompt' => 'smoothing, two people, disproportionate,  lowres, bad anatomy, bad hands, by bad artist, text,  missing fingers, extra digit,  jpeg artifacts,  watermark, blurry, artist name, fat, obese, extra fingers, mutated, (poorly drawn), (ugly),  (bad anatomy), (bad proportions), (extra limbs), 3d, cgi, render,  underexposed, facial markings, nipples, naked, modern, jeans, dark, child',
        'prompt' =>'modelshoot style, professional photograph, two thirds rule, Unreal Engine 4k, intricate details, dreamlikeart, octane render, masterpiece, award winning dslr raw photo, medieval, rpg, headshot, sexy.  (ragged,  sweaty,  ripped, weathered, filthy, dirty, disheveled, worn, muddy, torn clothes), headshot (moody:0.6) (gritty:0.3)  (nsfw:0.7)',
        'restore_faces'=>true,
        'steps' => 30,
        'seed'=>-1, // we can store this to regenerate the same character with different levels of damage, background ,etc
        'cfg_scale' => 9.5,
        'sampler_name' => 'Euler',
        'eta' => 0.3,

    ];

    public $location = [
        'height' => 320,
        'width' => 768,
        'negative_prompt' => 'worst quality, low quality, normal quality, jpeg artifacts, signature, watermark, username, blurry, artist name, (ugly), blurry, dull, uninteresting, cliche, uninspired, overexposed, over saturated, unrealistic, generic, amateruish, text, logo, worst quality, low quality, medium quality, deleted, lowres, comic, (watermarks), modern, streetlights, painting, pencil, cgi, 3d render',
        'prompt' => 'dslr raw photo, hdr, masterpiece, award winning professional photograph, fantasy landscape, rule of thirds,  digital art, Unreal Engine 4k, octane render,  landscape photography , cinematic lighting, volumetric lighting ,  ambient occlusion, particles,  (masterpiece, Ultra-detailed, Aperture = f/11, ISO = 100, Shutter_Speed = 1/125,HDR:1.3), rpg, medieval, Intricate, High Detail, dramatic, flickr, unsplash, bokeh, , wallpaper, nature',
        'restore_faces'=>false,
        'steps' => 30,
        'seed'=>-1, // we can store this to regenerate the same character with different levels of damage, background ,etc
        'cfg_scale' => 11.5,
        'hr_scale' => 2,
        'denoising_strength'=> 0.5,
        'enable_hr'=>true,
        'sampler_name' => 'Euler',
        'eta' => 0.3,

    ];

    //
    // for wounding characters: keep same seed, add this to varying degrees from 1 to 1.8 - past that and its cheesy horror
    //  (blood, bruised, cut, bleeding, beaten, battered, scarred, scratched, dying:1.2), (broken:1.4), (mangled:1.6), (disfigured:1.8)
    // lora seems to help somewhat in keeping it natural, not cartoonish : <lora:LoraHorrorS4w3d0ff_v10:0.5>
    // (blood, bruised, cut, bleeding, beaten, battered, scarred, scratched:1.1)(ragged, weathered,filthy, dirty, sweaty, worn, ripped, torn clothes:1.3),
    // these seem to give it a decent 'ruggedness' without overwhelming, increast blood from 1.1 to 1.6 for more gore, 1.8 for horror/dead

    public function await($params)
    {
        $id = $this->generate($params);

        $data = false;
        $startTime = microtime(1);
        $timeout = 125;
        while($data==false){
            usleep(300 *1000);
            Log::debug('Checking if image ready');
            $response = Http::withHeaders(['Authorization'=>'Bearer '.env('SD_API_KEY')])->get(env('SD_URL').'/status/'.$id)->json();
            Log::debug($response);
            if(!empty($response['output']['images'][0])) {
                $data = $response['output']['images'][0];
                Log::debug('Found image data');
            }
            $taken = microtime(1) - $startTime;
            if ($taken > $timeout) throw new \Exception('Retrieving image timed out');
        }

        return $response['output'];

    }

    public function generate($params)
    {
        Log::debug('Generating image using prompt:'.$params['prompt']);
        $response = Http::withHeaders(['Authorization'=>'Bearer '.env('SD_API_KEY')])->post(env('SD_URL').'/run',$params)->json();
        Log::debug('SD Response: '.json_encode($response));
        $id = $response['id'];
//        dump($id);
        Log::debug('SD ID: '.$id);
        return $id;

    }

    public function generateSync($params)
    {
        $response = Http::post(env('SD_URL').'/sdapi/v1/txt2img',$params)->json();
//        dump($response);
        $data = $response['images'][0];
        return $response;
    }

    public function getImage($params)
    {
        if (stripos(env('SD_URL'), 'localhost') !== false) {
            $data = $this->generateSync($params);
        } else {
            $data = $this->await(['input'=>$params]);
        }
        $imagedata = $data['images'][0];
        // now use Storage to save the base64 image string to disk
        $image = base64_decode($imagedata);
        // could use a better filename, but this will do
        $filename = Str::random(32).'.png';
        // may want to swap for shdw/s3/r2 at some point
        Log::debug('storing image to disk');
        Storage::disk('public')->put($filename, $image);
        // todo: convert filename to url
        $data['filename'] = $filename;
        return $data;

    }

    public function generateCharacter($character)
    {

        $params = $this->character;
        // prompt should now be:
        // Name, Race, illustration_description, params['prompt']

        $params['prompt'] = $character->attributes['name'].', '.$character->attributes['race'].', '.$character->attributes['illustration_description'].', '.$params['prompt'];

        if (strtolower($character->attributes['gender']) == 'female') $params['prompt'].=' (cleavage:1.1)';
        else $params['prompt'].=' (muscular)';

        $params['prompt'] = $character->prompt . ' ' . $params['prompt'];
        if($character->seed)     $params['seed'] = $character->seed;

        return $this->getImage($params);

    }

    public function generateLocation($location_prompt)
    {

        $params = $this->location;
        $params['prompt'] = $location_prompt . ' ' . $params['prompt'];


        return $this->getImage($params);
    }

}
