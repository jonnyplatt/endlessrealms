<?php

namespace App\Utils;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class DreamLike
{

//    public $baseStyle = 'pixel art, Lucasarts, Loom, Monkey Island, Simon The Sorcerer, Kyrandia, dark, moody, atmospheric, beautiful, Thimbleweed Park';
    public $baseStyle = 'stunning 3d render, digital art, highly detailed, Unreal Engine 4k, octane render';

    public function __construct()
    {
        if (!empty(Cache::tags([request()->ip()])->get('artModifier'))) {
            $this->baseStyle = Cache::tags([request()->ip()])->get('artModifier');
        }
    }

    public function await($prompt, $ratio = 'desktop', $negative='')
    {
        $status = 'waiting';
        $info = $this->dream($prompt, $ratio, $negative)->json();
        while($status !== 'completed') {
            $info = $info['dreams'][0];
            $status = $info['status'];
            $id = $info['id'];
            if(!empty($info['imageUrl'])) {
                return $info['imageUrl'];
            }
//            echo '.';
            sleep(1);
//            dump($info,$id);
            $info = $this->getUrl($id)->json();
        }
    }

    public function dream($prompt, $ratio = 'desktop', $negative = '')
    {
        $post ='{"model":
        {"name":"sd","version":"dreamlike-diffusion-1.0"},
        "parameters":
        {"prompt":"'.str_replace('"','\"',$prompt.' '.$this->baseStyle).'","negativePrompt":"out of frame, duplicate, watermark, signature, text '.$negative.'","scale":10.5,"strength":0.75,"aspectRatio":"'.$ratio.'","seed":'.random_int(0,99999999).',"sampler":"ddim","aestheticsFixFactor":0,"promptPreset":"none","steps":17,"imagesToGen":1},"isPublic":false, "wait":true}';

        $response = Http::withHeaders(['Authorization'=>'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdXRoUHJvdmlkZXIiOiJnb29nbGUiLCJpZFRva2VuIjoiZXlKaGJHY2lPaUpTVXpJMU5pSXNJbXRwWkNJNklqVTFNbVJsTWpkbU5URTFOek0zTlRNNU5qQXdaRGc1WWpsbFpUSmxOR1ZrTlRNMVptSTFNVGtpTENKMGVYQWlPaUpLVjFRaWZRLmV5SnBjM01pT2lKb2RIUndjem92TDJGalkyOTFiblJ6TG1kdmIyZHNaUzVqYjIwaUxDSmhlbkFpT2lJME1EWTVOVGMwTVRnNE5qTXRNMkZ3WVcwNWRHNWlOMmR0TUdodmNuWTBaM1UxYkhJd2JIQnVOMk5rWXpjdVlYQndjeTVuYjI5bmJHVjFjMlZ5WTI5dWRHVnVkQzVqYjIwaUxDSmhkV1FpT2lJME1EWTVOVGMwTVRnNE5qTXRNMkZ3WVcwNWRHNWlOMmR0TUdodmNuWTBaM1UxYkhJd2JIQnVOMk5rWXpjdVlYQndjeTVuYjI5bmJHVjFjMlZ5WTI5dWRHVnVkQzVqYjIwaUxDSnpkV0lpT2lJeE1UUTJNek13TURNME1qa3hNemN5TURjd09ETWlMQ0psYldGcGJDSTZJbXB2Ym01NWNHeGhkSFJBWjJsMlpXRnplVzkxWjJWMExtNWxkQ0lzSW1WdFlXbHNYM1psY21sbWFXVmtJanAwY25WbExDSmhkRjlvWVhOb0lqb2ljbGg1WTFaSFEweExaSFJMUVZoWWNVZHpkMWh3ZHlJc0ltNWhiV1VpT2lKS2IyNXVlU0JRYkdGMGRDSXNJbkJwWTNSMWNtVWlPaUpvZEhSd2N6b3ZMMnhvTXk1bmIyOW5iR1YxYzJWeVkyOXVkR1Z1ZEM1amIyMHZZUzlCUldSR1ZIQTNWell0UW5SSE9FRTFPVkZSVDNsRU16UkpWMkYyUkRrNExXdENSVFpuVDJkU1lXSmZSRmx1V1Qxek9UWXRZeUlzSW1kcGRtVnVYMjVoYldVaU9pSktiMjV1ZVNJc0ltWmhiV2xzZVY5dVlXMWxJam9pVUd4aGRIUWlMQ0pzYjJOaGJHVWlPaUpsYmkxSFFpSXNJbWxoZENJNk1UWTNNRGd6T1RBMU1pd2laWGh3SWpveE5qY3dPRFF5TmpVeWZRLm5sS1pkeDllQ1lGYllWUkZRbTd1ek9IREVIN3VvVFNFZkFFMFVTYkliU3RMcW5GR2U0Zl9tV2d2ZGZGWDNWXzJyTTNMMFBONkYxeHEwREo0WlctM0FlMU9rZmdCVjlhS2tWVS1fRWQyZUFXOHBzclA3SHliWkNmdlAxU2VMWk9BVEdBUkpmbWhCM2x1UHdQOUxmR29lZWZ4enFqZ3dEY1h1bkI2RGNCcUlvejh3Y3ZzVS12SFgxXzhKdWlSTnpmdmdlWGhvMlRUNl9wbkticWxrcy1yUG03dHZBUWt3QkZBY3BodUtWMnUwREM1QjdvRk9aNEE1amF4aGxxSFVKTTFQYlY3eTVaLXlLc1VfaHFGXzR5emNQdk0xdWpnc0JpWlFxUFpQczhSMVhjMURrUTFpdG13T2pzSEhnd1kyZk94OHpmVFlmb3l2ZjlNeDlFUmprZGZQZyIsInVzZXIiOiIxMDA1YmJmYi1lZjg1LTQ3YTctYTlmYy1iN2MyMmY0MGIxMmMiLCJpYXQiOjE2NzA4MzkwNTJ9.QpOESAt1rB3JGqrFnVazpWThqnD2GSqvNYSS1Tyr4tw'])->post('https://api.dreamlike.art/v1/dreams/dream', json_decode($post));
//        dump($response->json());
        Log::debug($response->body());
        if(empty($response->json()['dreams'][0]['_id'])) dump($response->json(),json_decode($post),$prompt.' '.$this->baseStyle);
        $id = $response->json()['dreams'][0]['_id'];
//          dump($id);
          sleep(2);
          return $this->getUrl($id);

    }

    public function getUrl($id)
    {
        $response = Http::withHeaders(['Authorization'=>'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdXRoUHJvdmlkZXIiOiJnb29nbGUiLCJpZFRva2VuIjoiZXlKaGJHY2lPaUpTVXpJMU5pSXNJbXRwWkNJNklqVTFNbVJsTWpkbU5URTFOek0zTlRNNU5qQXdaRGc1WWpsbFpUSmxOR1ZrTlRNMVptSTFNVGtpTENKMGVYQWlPaUpLVjFRaWZRLmV5SnBjM01pT2lKb2RIUndjem92TDJGalkyOTFiblJ6TG1kdmIyZHNaUzVqYjIwaUxDSmhlbkFpT2lJME1EWTVOVGMwTVRnNE5qTXRNMkZ3WVcwNWRHNWlOMmR0TUdodmNuWTBaM1UxYkhJd2JIQnVOMk5rWXpjdVlYQndjeTVuYjI5bmJHVjFjMlZ5WTI5dWRHVnVkQzVqYjIwaUxDSmhkV1FpT2lJME1EWTVOVGMwTVRnNE5qTXRNMkZ3WVcwNWRHNWlOMmR0TUdodmNuWTBaM1UxYkhJd2JIQnVOMk5rWXpjdVlYQndjeTVuYjI5bmJHVjFjMlZ5WTI5dWRHVnVkQzVqYjIwaUxDSnpkV0lpT2lJeE1UUTJNek13TURNME1qa3hNemN5TURjd09ETWlMQ0psYldGcGJDSTZJbXB2Ym01NWNHeGhkSFJBWjJsMlpXRnplVzkxWjJWMExtNWxkQ0lzSW1WdFlXbHNYM1psY21sbWFXVmtJanAwY25WbExDSmhkRjlvWVhOb0lqb2ljbGg1WTFaSFEweExaSFJMUVZoWWNVZHpkMWh3ZHlJc0ltNWhiV1VpT2lKS2IyNXVlU0JRYkdGMGRDSXNJbkJwWTNSMWNtVWlPaUpvZEhSd2N6b3ZMMnhvTXk1bmIyOW5iR1YxYzJWeVkyOXVkR1Z1ZEM1amIyMHZZUzlCUldSR1ZIQTNWell0UW5SSE9FRTFPVkZSVDNsRU16UkpWMkYyUkRrNExXdENSVFpuVDJkU1lXSmZSRmx1V1Qxek9UWXRZeUlzSW1kcGRtVnVYMjVoYldVaU9pSktiMjV1ZVNJc0ltWmhiV2xzZVY5dVlXMWxJam9pVUd4aGRIUWlMQ0pzYjJOaGJHVWlPaUpsYmkxSFFpSXNJbWxoZENJNk1UWTNNRGd6T1RBMU1pd2laWGh3SWpveE5qY3dPRFF5TmpVeWZRLm5sS1pkeDllQ1lGYllWUkZRbTd1ek9IREVIN3VvVFNFZkFFMFVTYkliU3RMcW5GR2U0Zl9tV2d2ZGZGWDNWXzJyTTNMMFBONkYxeHEwREo0WlctM0FlMU9rZmdCVjlhS2tWVS1fRWQyZUFXOHBzclA3SHliWkNmdlAxU2VMWk9BVEdBUkpmbWhCM2x1UHdQOUxmR29lZWZ4enFqZ3dEY1h1bkI2RGNCcUlvejh3Y3ZzVS12SFgxXzhKdWlSTnpmdmdlWGhvMlRUNl9wbkticWxrcy1yUG03dHZBUWt3QkZBY3BodUtWMnUwREM1QjdvRk9aNEE1amF4aGxxSFVKTTFQYlY3eTVaLXlLc1VfaHFGXzR5emNQdk0xdWpnc0JpWlFxUFpQczhSMVhjMURrUTFpdG13T2pzSEhnd1kyZk94OHpmVFlmb3l2ZjlNeDlFUmprZGZQZyIsInVzZXIiOiIxMDA1YmJmYi1lZjg1LTQ3YTctYTlmYy1iN2MyMmY0MGIxMmMiLCJpYXQiOjE2NzA4MzkwNTJ9.QpOESAt1rB3JGqrFnVazpWThqnD2GSqvNYSS1Tyr4tw'])->post('https://api.dreamlike.art/v1/dreams/get-many',[ 'ids' => [$id]]);
//        dump($response);
        return $response;
    }
}
