<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreQuestRequest;
use App\Http\Requests\UpdateQuestRequest;
use App\Models\Character;
use App\Models\Quest;
use App\Models\Turn;
use App\Models\World;
use App\Utils\DreamLike;
use App\Utils\RunPod;
use App\Utils\StoryGenerator;
use Illuminate\Support\Facades\Cache;

class QuestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return inertia('Story/Begin',[
            'worlds' => Cache::get('worlds'),
        ]);
    }

    public function begin(Character $character)
    {
        $storyGenerator = new StoryGenerator();
        $world = World::find(1);

        $history = $character->turns();


        $turn = $storyGenerator->typescriptTurn($world, $character, $history, "Begin Quest ".$world->quest_reason);

        return inertia('Story/Story', [
            'world' => $world,
            'character' => $character,
            'history' => $history,
            'turn' => null,
        ]);

    }

    public function turn(Character $character)
    {
        $action = request()->input('action');
        $world = $character->world;
        $history = $character->turns();
        $storyGenerator = new StoryGenerator();
        $turn = $storyGenerator->typescriptTurn($world, $character, $history, $action);

        return inertia('Story/Story', [
            'world' => $world,
            'character' => $character,
            'history' => $history,
            'turn' => $character->turns()->latest()->first(),
        ]);

    }

    public function current(Character $character)
    {
        $world = $character->world;
        $history = $character->turns();
        $turn = $character->turns()->latest()->first();
        return inertia('Story/Story', [
            'world' => $world,
            'character' => $character,
            'history' => $history,
            'turn' => $turn,
        ]);
    }

    public function getCharacterImage(Character $character)
    {
        $world = World::find(1);
        // this should be stored to db now, no?
        if(!empty($character->image)) {
            return response()->json(['url' => env('APP_URL').$character->image]);
        }

    }

    public function getNpcImage(Turn $turn)
    {
        // if we have the npc image already, return it
        if ($turn->npc_image) {
            return response()->json(['url' => $turn->npc_image]);
        }
        // otherwise, generate it from turn data
        $npc_object = collect($turn->turn_response['location']['NPCs'])->first();

        // no npcs here? return. this should be handled in ui
        if(empty($npc_object)) {
            return response()->json(['url' =>false]);
        }
        $location_object = $turn->turn_response['location'];
        $location_desc = " in {$location_object['description']['name']}) ({$location_object['description']['illustration']}";
        // create a character object from the npc data
        $npc = new Character();
        $npc->attributes = [
            'name' => $npc_object['description']['name'],
            'illustration_description' => '('.$npc_object['description']['illustration'] . ':1.2) '.$location_desc,
            'race'=>'', // should be in name
            'gender'=>'', // should be in name
        ];
        $rp = new RunPod();
        // generate a character image
        $data = $rp->generateCharacter($npc);

        // save back to turn
        $turn->npc_image = '/'.$data['filename'];
        $turn->save();

        return response()->json(['url' => env('APP_URL').$data['filename']]);

    }

    public function getBackgroundImage(Turn $turn)
    {
        // if we have the location_image already, return it
        if ($turn->location_image) {
            return response()->json(['url' => $turn->location_image]);
        }
        // otherwise, generate it from turn data
        $location_object = $turn->turn_response['location'];
        // create a prompt string from the location name and description
        $prompt = "({$location_object['description']['name']}) ({$location_object['description']['illustration']})";

        $rp = new RunPod();
        // generate a location image
        $data = $rp->generateLocation($prompt);

        // save back to turn
        $turn->location_image = env('APP_URL').$data['filename'];
        $turn->save();

        return response()->json(['url' => env('APP_URL').$data['filename']]);

    }
}
