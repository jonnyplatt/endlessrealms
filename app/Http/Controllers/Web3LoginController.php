<?php

namespace App\Http\Controllers;

use App\Models\User;
use BenjaminStout\PHPCrypt\Crypt;
use Elliptic\EC;
use Elliptic\EdDSA;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Spatie\Crypto\Rsa\PublicKey;
use StephenHill\Base58;

class Web3LoginController extends Controller
{
    public function getLoginMessage()
    {

        $message="Sign this message to login and start playing.\n".session()->token();
        session()->put('login_message', $message);
        return $message;

    }

    public function isLoggedIn()
    {
        return auth()->user();
    }

    public function logout()
    {
        auth()->logout();
        // clear the session
        session()->invalidate();
        return true;
    }

    public function signatureLoginOrRegister()
    {
        $pubkey = request()->input('publicKey'); // needs to be a hex string, not base58. encoding currently done client side
        $signature =  request()->input('signature'); // signature is a hex string

//        $message=bin2hex("Hello World");
        $message = bin2hex(session()->get('login_message'));

        $str = '{"signature":"' . $signature . '"}';

        $ec =  new EdDSA('ed25519');
        $key = $ec->keyFromPublic($pubkey);

        $success = $key->verify($message, $signature);

        if(!$success){
            // log out any user that might be logged in
            auth()->logout();
            // clear the session
            session()->invalidate();

            return response()->json(['message' => 'Invalid signature'], 403);
        }

        $user = User::where('public_key', $pubkey)->first();
        if(!$user) {
            $user = new User([
                'public_key' => $pubkey,
                'name' => $pubkey,
                'email' => '',]);
            Log::debug($user);
            $user->Save();
        }
        // login user
        auth()->login($user);
        return response()->json(['message' => 'Login successful','user' => $user]);

    }
    public function unicodeString($str, $encoding=null) {
        if (is_null($encoding)) $encoding = ini_get('mbstring.internal_encoding');
        return preg_replace_callback('/\\\\u([0-9a-fA-F]{4})/u', create_function('$match', 'return mb_convert_encoding(pack("H*", $match[1]), '.var_export($encoding, true).', "UTF-16BE");'), $str);
    }
}
