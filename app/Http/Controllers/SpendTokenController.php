<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Tighten\SolanaPhpSdk\Connection;
use Tighten\SolanaPhpSdk\Keypair;
use Tighten\SolanaPhpSdk\Programs\SplTokenProgram;
use Tighten\SolanaPhpSdk\PublicKey;
use Tighten\SolanaPhpSdk\SolanaRpcClient;
use Tighten\SolanaPhpSdk\Transaction;
use Tighten\SolanaPhpSdk\TransactionInstruction;
use Tighten\SolanaPhpSdk\Util\AccountMeta;

class SpendTokenController extends Controller
{
    public function spend()
    {
        $token = request()->input('token');
        $owner = request()->input('owner');
        $amount = request()->input('amount');
        $fromAccount = request()->input('fromAccount');

        // load our Secret Key
        $keypair = json_decode(file_get_contents(base_path('delegate.json')));
        $delegateKey = Keypair::fromSecretKey($keypair);

        // check if we have an associated token account
//        $associatedTokenAccount = $this->getAssociatedTokenAccount($delegateKey->getPublicKey(), $token);
        // todo: send the token to a cold wallet's ATA, not the delegate's ATA. This way delegate auth can live on server but tokens can live in a secure offline wallet
        $associatedTokenAccount = new PublicKey('2LKQ55oVuhugHeoYTPPduyjsdx1wDkH3WRZgdLiqL9w7');
        // ideally should check if the account is empty and if so, create it, but for now we'll just assume it exists
        // and if it doesn't, the transaction will fail
//        dd($associatedTokenAccount->toBase58());
        // now we need to create the transaction to send the tokens to our associated token account
        $transaction = new Transaction(null,null,$delegateKey->getPublicKey());
        $transaction->add(
           $this->createTransferCheckedInstruction(
               $fromAccount,
               $associatedTokenAccount,
               $delegateKey->publicKey, // in this case, an owner is actually the signer
               $token,
               $amount * 10**9,
               9
           )

        );
//        dump($transaction);
        $client = new SolanaRpcClient('https://try-rpc.mainnet.solana.blockdaemon.tech/');
        $connection = new Connection($client);
        $txHash = $connection->sendTransaction($transaction, [$delegateKey]);
        return $txHash;
    }

    public function createTransferCheckedInstruction(
        string $source,
        string $destination,
        string $owner,
        string $mint,
        int    $amount,
        int    $decimals
    ): TransactionInstruction
    {
        /**
         * TokenInstructions :
         *   Transfer = 3,
         *   TransferChecked = 12,
         */
        $data = [
            // u8
            ...unpack("C*", pack("C", 12)),  // token instruction index
            // u64
            ...unpack("C*", pack("P", $amount)), // amount without decimals
            // u8
            ...unpack("C*", pack("C", $decimals)),   // token decimals
        ];
        $keys = [
            new AccountMeta(new PublicKey($source), false, true), // source account address
            new AccountMeta(new PublicKey($mint), false, false), // mint token address
            new AccountMeta(new PublicKey($destination), false, true), // destination account address, if not exist need call
            new AccountMeta(new PublicKey($owner), true, true), // singer address, wallet who will be sing tx
        ];

        return new TransactionInstruction(
            new PublicKey(SplTokenProgram::SOLANA_TOKEN_PROGRAM_ID),
            $keys,
            $data
        );
    }

    public static function getAssociatedTokenAccount(string $mint, string $publicKeyAddress): PublicKey
    {
        $publicKeyAccount = new PublicKey($publicKeyAddress);
        $publicKeyMint = new PublicKey($mint);
        $programId = new PublicKey("TokenkegQfeZyiNwAJbNbGKPFXCWuBvf9Ss623VQ5DA");
        $seed = [
            $publicKeyAccount->toBuffer(),
            $programId->toBuffer(),
            $publicKeyMint->toBuffer()
        ];
        // ATTENTION : It's function not working correctly in latest version of package, now latest is "^0.3.2", for solve it change package version to "dev-main
        return Arr::first(PublicKey::findProgramAddress($seed, new PublicKey("ATokenGPvbdGVxr1b2hvZbsiqW5xWH25efTNsLJA8knL")));
    }

    public static function createAssociatedTokenAccountInstruction(
        string $payer, // tx payer address. it address wallet whose using for sining transaction
        string $associatedToken, // generated token address, for get call function "getAssociatedTokenAccount"
        string $owner, //  wallet address for which you want to create associated token wallet
        string $mint,   // Token mint address
        string $programId = "TokenkegQfeZyiNwAJbNbGKPFXCWuBvf9Ss623VQ5DA",
        string $associatedTokenProgramId = "ATokenGPvbdGVxr1b2hvZbsiqW5xWH25efTNsLJA8knL"
    ): TransactionInstruction
    {
        $data = [];
        $keys = [
            new AccountMeta(new PublicKey($payer), true, true),
            new AccountMeta(new PublicKey($associatedToken), false, true),
            new AccountMeta(new PublicKey($owner), false, false),
            new AccountMeta(new PublicKey($mint), false, false),
            new AccountMeta(new PublicKey("11111111111111111111111111111111"), false, false),
            new AccountMeta(new PublicKey($programId), false, false),
            new AccountMeta(new PublicKey("SysvarRent111111111111111111111111111111111"), false, false),
        ];

        return new TransactionInstruction(
            new PublicKey($associatedTokenProgramId),
            $keys,
            $data
        );
    }

    public function confirmSuccessfulTransfer($txHash, $amount)
    {
        $client = new SolanaRpcClient('https://try-rpc.mainnet.solana.blockdaemon.tech/');
        $connection = new Connection($client);
        $transaction = $connection->getConfirmedTransaction($txHash);
        dump($transaction);

        if ($transaction['meta']['err'] !== null) {
            // transaction failed
            throw new \Exception('Transaction failed: ' . $transaction['meta']['err']['InstructionError'][1]);

        }

        $preTokenBalance = $transaction['meta']['preTokenBalances'][1]['uiTokenAmount']['amount'];
        $postTokenBalance = $transaction['meta']['postTokenBalances'][1]['uiTokenAmount']['amount'];

        if ($postTokenBalance - $preTokenBalance === $amount) {
            // transaction successful
            return $amount;
        } else {
            // amounts incorrect, fail transaction
            throw new \Exception('Transaction amounts incorrect - expected ' . $amount . ' but got ' . ($postTokenBalance - $preTokenBalance).'. Transaction hash: '.$txHash.' Pre token balance: '.$preTokenBalance.' Post token balance: '.$postTokenBalance);
        }


    }


}
