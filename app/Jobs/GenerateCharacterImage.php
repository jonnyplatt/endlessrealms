<?php

namespace App\Jobs;

use App\Models\Character;
use App\Utils\RunPod;
use App\Utils\StoryGenerator;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class GenerateCharacterImage implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private Character $character;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Character $character)
    {
        //
        $this->character = $character;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $sg = new StoryGenerator();
        $rp = new RunPod();
        $data = $rp->generateCharacter($this->character);
        $character = $this->character;
        $attrs = $character->attributes;
        if (empty($attrs['images'])) {
            $attrs['images'] = [];
        }
        $info = json_decode($data['info'], true);
        $attrs['images'][] = [
            'filename'=> $data['filename'],
            'seed'=> $info['seed'],
            'desc'=> $character->attributes['name'].', '.$character->attributes['race'].', '.$character->attributes['illustration_description']
        ];
        $character->image = $data['filename'];
        $character->attributes = $attrs;
        $character->save();

    }
}
