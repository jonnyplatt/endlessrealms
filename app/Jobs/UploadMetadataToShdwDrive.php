<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class UploadMetadataToShdwDrive implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $path;
    private $filename;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($path, $filename)
    {
        //
        $this->path = $path;
        $this->filename = $filename;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // upload using shdw drive cli tool
        $args = '-kp '. env('SHDW_DRIVE_KEYPAIR')
            .' -f '. $this->path
            .' -s '. env('SHDW_DRIVE_STORAGE_ACCOUNT')
            .' -r '. env('SHDW_DRIVE_RPC');

        // first delete the file if it exists
        $command = 'shdw-drive delete-file '.$args;
        dump($command);
        dump(shell_exec($command));

        // then upload the file
        $command = 'shdw-drive upload-file '.$args;
        dump($command);
        dump(shell_exec($command));


    }
}
