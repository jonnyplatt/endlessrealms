<?php

namespace App\Jobs;

use App\Utils\DreamLike;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Cache;

class GenerateImage implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $type;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($type)
    {
        //
        $this->type = $type;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $turn = Cache::tags([request()->ip()])->get('last_turn');
        $character = Cache::tags([request()->ip()])->get('character');
        $world = Cache::tags([request()->ip()])->get('world');

        $dream = new DreamLike();

        if ($this->type == 'character') {
            $prompt = "({$character->illustration}), character profile,".$world->character_art_style;
            $url = $dream->await($prompt,'square');
            // check if character has changed
            $character = Cache::tags([request()->ip()])->get('character');
            $character->image_url = $url;
            Cache::tags([request()->ip()])->put('character', $character);

        } else if ($this->type=='npc') {
            $prompt = "({$turn->n_p_c_illustration}), character profile,".$world->character_art_style;
            $url = $dream->await($prompt,'square');
            // check if turn has changed
            $turn = Cache::tags([request()->ip()])->get('last_turn');
            $turn->npc_image_url = $url;
            Cache::tags([request()->ip()])->put('last_turn', $turn);
        } else if ($this->type=='background') {
            $prompt = "({$turn->location}) ({$turn->illustration}), background,".$world->background_art_style;
            $url = $dream->await($prompt,'desktop');
            // check if turn has changed
            $turn = Cache::tags([request()->ip()])->get('last_turn');
            $turn->background_image_url = $url;
            Cache::tags([request()->ip()])->put('last_turn', $turn);
        }

    }
}
