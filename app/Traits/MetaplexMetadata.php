<?php

namespace App\Traits;

use App\Jobs\UploadMetadataToShdwDrive;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Storage;

trait MetaplexMetadata
{

    public function getPropertiesAttribute()
    {
        $attributes = json_decode($this->attributes['attributes'],1);
//        dump($attributes);
        $properties = [
            'files' =>
                (collect($attributes['images'])->reverse() ?? collect() )->map(function ($image) {
                    return [
                        'uri' => env('APP_URL').$image['filename'],
                        'type' => 'image/jpeg',
                    ];
                })->values()->toArray(),

            'category' => 'image',
            'creators' => [
                [
                    'address' => env('METAPLEX_CREATOR_ADDRESS'),
                    'share' => 100,
                ],
            ],
        ];

        if ($this->creator) {
            $properties['creators'][] = [
                'address' => $this->creator->address,
                'share' => 50,
            ];
            $properties['creators'][0]['share'] = 50;
        }

        return $properties;
    }

    public function metadataJson()
    {
        $attr = json_decode($this->attributes['attributes'],1);
        // map the model properties to an array of attributes eg [ ['property','value'], ['property','value'] ]
        $attributes = collect(
            Arr::dot($attr)
            )->map(function ($value, $key) {
            return ['trait_type' => $key, 'value' => (string) $value];
        })->values()->toArray();


        $arr = [
            'name' => $attr['name'],
            'symbol' => '',
            'description' => $attr['backstory'] ?? '',
            'seller_fee_basis_points' => $this->seller_fee_basis_points,
            'image' => env('APP_URL').$this->image,
            'external_url' => env('APP_URL').'/character/' . $this->id,
            'attributes' => $attributes,
            'properties' => $this->properties,
            'collection' => [
                'name' => $this->world->name,
                'family' => 'Endless Realms',
            ],
        ];
        dump($arr);
        return json_encode($arr);
    }

    // override the default save method to save the metadata json to the filesystem too
    public function save(array $options = [])
    {
        $respones = parent::save($options);
//        $this->saveMetadata();


        return $respones;
    }

    // save the metadata json to the filesystem
    public function saveMetadata()
    {
        $filename = $this->id . '.json';
        $path = 'metadata/' . $filename;
        Storage::put($path, $this->metadataJson());
        Storage::disk('s3')->put($filename, $this->metadataJson());
        // dispatch job to upload to shdw-drive
//        UploadMetadataToShdwDrive::dispatch($path, $filename);

    }

}
