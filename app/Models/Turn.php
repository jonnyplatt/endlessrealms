<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Turn extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $casts =[
        'player_state'=>'array',
        'turn_response'=>'array',
    ];
}
