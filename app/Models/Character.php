<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Schema\Blueprint;
use App\Traits\MetaplexMetadata;

class Character extends Model
{
    use MetaplexMetadata;

    public static $driver = 'json';

    protected $casts = [
        'attributes' => 'array',
        'properties' => 'array',
    ];

    protected $guarded = [];

    public function world()
    {
        return $this->belongsTo(World::class);
    }

    public function turns()
    {
        return $this->hasMany(Turn::class)->orderByDesc('turns.id');
    }



}
