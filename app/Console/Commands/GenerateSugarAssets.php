<?php

namespace App\Console\Commands;

use App\Models\Character;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class GenerateSugarAssets extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sugar:assets';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // copy all character metadatas to the assets folder
        try {
            Storage::createDirectory('sugar');
        } catch (\Exception $e) {
        }
        foreach(Character::all() as $character) {
            dump( storage_path('app/public/'.$character->image), storage_path('app/sugar/'.$character->id.'.png'));

            dump(Storage::copy( 'public/'.$character->image, 'sugar/'.$character->id.'.png'));
            Storage::copy( 'metadata/'.$character->id.'.json', 'sugar/'.$character->id.'.json');
        }


        return Command::SUCCESS;
    }
}
