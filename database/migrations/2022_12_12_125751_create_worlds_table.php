<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('worlds', function (Blueprint $table) {
            $table->id();

            /* we currently have this data for a world
            [▼
    "theme" => "a futuristic and moody cyberpunk world"
    "world_name" => "The Nexus"
    "starting_city" => "Red Drift"
    "world_setting" => "The Nexus is a sprawling cyberpunk metropolis, where advanced technology and urban decay blend together. Surrounded by an endless expanse of desert, the city is ▶"
    "art_style" => "Grim, Futuristic, Cyberpunk, Dark, Gloomy"
    "character_art_style" => "Stylized, Gritty, Cyberpunk, Edgy, Moody"
    "your_quest" => "Our adventurers must travel across The Nexus in search of an ancient artifact known only as the Ark. The Ark is rumored to hold the power to unlock great secrets hidden within the depths of the city. The adventurers must brave the dangers of The Nexus in order to find the Ark and unlock its secrets.  ◀"
    "goal_name" => "The Ark "
    "goal_destination" => "The hidden depths of The Nexus "
    "goal_reason" => "To unlock great secrets and power "
    "route" => "The adventurers will begin their journey at Red Drift, a bustling neon-lit port in the heart of The Nexus. From there they will venture deeper into the city, exploring its forgotten streets and dark corners. Along the way they will encounter a variety of challenges, from hostile street gangs to powerful corporate executives. They will also discover a number of hidden passageways that lead to long-forgotten ruins and forgotten technologies. Finally, they must brave the deepest depths of The Nexus if they wish to uncover the secrets of the Ark.  ◀"
    "places" => "Red Drift, Forgotten Streets, Dark Corners, Hostile Street Gangs, Corporate Executives, Hidden Passageways, Long-Forgotten Ruins, Forgotten Technologies "
    "challenges" => "Along their journey, our adventurers will face a variety of dangers and enemies. They will have to battle hostile street gangs and powerful corporate executives who are looking to exploit the system. They must also brave ancient ruins that are filled with traps and mysterious creatures. Finally, they must contend with ◀"
  ]
            */

            $table->string('theme');
            $table->string('world_name');
            $table->string('starting_city');
            $table->text('world_setting');
            $table->string('art_style');
            $table->string('character_art_style');
            $table->text('your_quest');
            $table->string('goal_name');
            $table->text('goal_destination');
            $table->text('goal_reason');
            $table->text('route');
            $table->text('places');
            $table->text('challenges');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('worlds');
    }
};
