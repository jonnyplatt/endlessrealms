<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('characters', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id')->nullable()->index();
            $table->integer('world_id')->nullable()->index();
            $table->integer('quest_id')->nullable();
            $table->integer('turn_id')->nullable();
            $table->string('mint_pubkey')->nullable()->index();



            // do we want to store our own data separately? Or does everything go into metaplex? Eg

            // onchain metadata
//        $table->string('name');
//        $table->string('symbol')->default('');
            // point to SHDW or server for now?
//        $table->string('uri')->default('');
            // seller fee basis points: set in mint function
            // creatros array: set in mint function
            // update authority: set in mint function

            // offchain metadata
//        $table->string('description')->default('');
            $table->integer('seller_fee_basis_points')->default(30);
            // keep primary image here for internal use
            $table->string('image')->default('');
            // $table->string('animation_url')->default('');
            // would be great to link this to a character profile page showing their personal history. But can be rendered, not stored in DB
            // $table->string('external_url')->default('');
            $table->json('attributes')->nullable();
            $table->json('properties')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('characters');
    }
};
