<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('turns', function (Blueprint $table) {
            $table->id();
            // what to save to a turn?
            // character_id
            // world_id
            // wallet_pubkey - nullable, allows us to track 'who' played each character
            // player_state json - track health, items etc at time of turn
            // turn_number - integer
            // turn_response json - what we get back from the AI. This includes summaries of past turns for history, changes to player, npc, location etc
            // Do we want to separate out location, NPC, player, item changes etc? Where will we store the npc/location images, especially if it hasn't changed yet?
            // I don't think it matters too much given we won't be using WHERE on the json
            // location_image - nullable, string -  we can recycle this for the next turn if location hasn't changed
            // npc_image - nullable, string - ditto

            $table->integer('character_id')->index();
            $table->integer('world_id')->index();
            $table->string('wallet_pubkey')->nullable()->index();
            $table->json('player_state')->nullable();
            $table->integer('dice_roll')->nullable();
            $table->integer('turn_number')->nullable();
            $table->json('turn_response')->nullable();
            $table->string('location_image')->nullable();
            $table->string('npc_image')->nullable();



            // is there anything else we need to be able to rebuild our prompts for later finetuning?




            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('turns');
    }
};
