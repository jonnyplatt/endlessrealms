<?php

use App\Http\Controllers\ProfileController;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});

Route::get('/story', function () {
    return Inertia::render('Story/Begin', [
        'worlds' => Cache::get('worlds'),
        'user' => \Illuminate\Support\Facades\Auth::user(),
    ]);
});

Route::get('/story/dump', function () {
    dump(Cache::tags([request()->ip()])->get('character'));
    dump(Cache::tags([request()->ip()])->get('history'));
    dump( Cache::tags([request()->ip()])->get('last_turn'));
    dump( Cache::tags([request()->ip()])->get('world'));
});
Route::get('/web3', function () {
    return Inertia::render('Login', [
    ]);
});

Route::get('/test', function () {
    return Inertia::render('TestTokenTransfer', [
    ]);
});
Route::post('/web3/spend',[\App\Http\Controllers\SpendTokenController::class, 'spend']);

Route::post('/web3',[\App\Http\Controllers\Web3LoginController::class, 'signatureLoginOrRegister']);
Route::get('/web3/message',[\App\Http\Controllers\Web3LoginController::class, 'getLoginMessage']);
Route::get('/web3/user',[\App\Http\Controllers\Web3LoginController::class, 'isLoggedIn']);
Route::get('/web3/logout',[\App\Http\Controllers\Web3LoginController::class, 'logout']);

Route::post('/story/begin/{character}', [\App\Http\Controllers\QuestController::class, 'begin']);
Route::post('/story/turn/{character}', [\App\Http\Controllers\QuestController::class, 'turn']);
Route::get('/story/turn/{character}', [\App\Http\Controllers\QuestController::class, 'current']);
Route::get('/story/begin/{character}', [\App\Http\Controllers\QuestController::class, 'begin']);
Route::get('/story/characterImage/{character}', [\App\Http\Controllers\QuestController::class, 'getCharacterImage']);
Route::get('/story/backgroundImage/{turn}', [\App\Http\Controllers\QuestController::class, 'getBackgroundImage']);


Route::get('/character/{character}', [\App\Http\Controllers\CharacterController::class, 'show']);

Route::get('/story/npcImage/{turn}', [\App\Http\Controllers\QuestController::class, 'getNpcImage']);

Route::get('/dashboard', function () {
    return Inertia::render('Dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

require __DIR__.'/auth.php';
