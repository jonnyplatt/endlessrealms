import { defineConfig } from 'vite';
import laravel from 'laravel-vite-plugin';
import vue from '@vitejs/plugin-vue';


import { NodeGlobalsPolyfillPlugin } from '@esbuild-plugins/node-globals-polyfill'
import nodePolyfills from 'rollup-plugin-node-polyfills';

export default defineConfig({
    plugins: [
        laravel({
            input: 'resources/js/app.js',
            refresh: true,
        }),
        vue({
            template: {
                transformAssetUrls: {
                    base: null,
                    includeAbsolute: false,
                },
            },
        }),
    ],
    define: {
        global: {},
        'process.env': {},
    },
    resolve: {
        alias: {
            stream: 'rollup-plugin-node-polyfills/polyfills/stream',
            events: 'rollup-plugin-node-polyfills/polyfills/events',
            assert: 'assert',
            crypto: 'crypto-browserify',
            util: 'util',
        },
    },
    build: {
        target: 'esnext',
        rollupOptions: {
            plugins: [
                nodePolyfills({ crypto: true }),
            ],
        },
    },
    // optimizeDeps: {
    //     esbuildOptions: {
    //         plugins: [
    //             NodeGlobalsPolyfillPlugin({ buffer: true }),
    //         ],
    //     }
    // },
});
