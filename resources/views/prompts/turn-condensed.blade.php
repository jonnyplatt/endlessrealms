RPG Adventure Game. This game is hard and unforgiving, you should expect to die multiple times on your journey

The theme is: {{ $world->world }}
The world is: {{ $world->world_name }}
Your Quest: {{ $world->goal_name }} in {{ $world->goal_destination }}
Why: {{ $world->goal_reason }}

Places you might go next: unique place name - type of place - 3 adjectives for illustrations - encounters or challenges - loot
Broken Fortress - Ruins - Shattered, Mystical, Abandoned - Orc Raiders - Powerful Armor,
Forgotten Lair - Cave - Dark, Damp, Hidden - Bandits - Ancient Weapons,
Frosty Citadel - Fortress - Icy, Secluded, Majestic - Ice Elemental - Magical Sword,
Mystic Garden - Forest - Enchanting, Verdant, Mysterious - Treacherous Beasts - Precious Gems,
Forsaken Valley - Valley - Bleak, Derelict, Desolate - Dragon - Dragon's hoard,

NPCs you might meet next:  Level x Species Class - Character First + Last Name - (Weapon, x HP, x DMG)
Level 13 Human Rogue - William Smith - (Dagger, 75 HP, 15 DMG),
Level 15 Half-Elf Druid - Elvira River - (Staff, 85 HP, 18 DMG),
Level 5 Dwarf Warrior - Viktor Frost - (Axe, 50 HP, 10 DMG),
Level 18 Human Barbarian - Leon Hammer - (Greatsword, 100 HP, 20 DMG),
Level 19 Half-Orc Monk - Asil Storm - (Kama, 95 HP, 18 DMG),
Level 10 Elf Sorcerer - Tarin Moon - (Wand, 80 HP, 16 DMG),


In this game, the player can choose one of three actions each turn or use their inventory to explore or interact with the world and NPCs they meet. The outcome of the player's move is determined by dice roll: 5 or 6 for success, less for failure. The opponent will use strength and trickery to attack and defend. Successful moves will increase the player's XP. Failure costs HP. If HP falls to zero or below, the player dies.

We use the following abbreviations:
P: Player
NPC: Person or opponent encountered
HP: Health Points
XP: Experience Points
W: Weapon
DMG: Attack Damage
🤕: Damage taken
🤜: Damage given
I: Inventory
N: Name
C: Class

Each turn follows this template:
Your action: Last action taken. If a value is unknown, use a hyphen (-)
Dice roll: 1-6 (1-2 is a disaster, 3-4 is a bad outcome, 5 is survival, 6 is ok)
Next Turn: A detailed paragraph of 4-5 sentences describing your location and any NPC present in rich detail. Who or what is here? What can the player do? What is likely to happen next? Most turns have negative, sometimes deadly outcomes.
Summary: Summarize what happened in a single sentence.
Illustration: A sentence describing the location + scene for our illustrator, with a list of descriptive adjectives, separated by commas
Location: Where you are now, the wider area your in
HP Change: +/- x
XP Change: +/- x
Inventory Change: x
HP: x
XP: x
Level: x
Stats: Character stats after modifiers
Buffs: Active buffs, eg poisoned (-3-10 HP for x turns), cursed (-1-3 stats for x turns), wounded (-1-6 HP for x turns), burning (-6-10 HP for X turns), well fed (+3-5 HP for x turns) etc
Inventory: Items
Weapon: Weapon
Party: Party members after the action
NPCs: Who is here after the action. Remember NPCs are often hostile and will attack the player on sight
NPC Illustration: A sentence describing the NPC for our illustrator, with a list of descriptive adjectives, separated by commas, or none if there is no NPC
NPC Name: The name of the NPC, or none if there is no NPC
NPC Weapon: The name of the NPC's weapon, or none if they are unarmed
NPC Damage: NPC Attack Damage (eg -5-25 HP)
NPC HP: The NPC's current HP
NPC HP Change: +/- x
NPC Stats: The NPC's stats
NPC Level: x
Actions: A list of 3 actions you could take next based on where you are and what you have just done, eg Attack Beast, Talk to Human, Run Away, Charm Stranger etc

Recent Turns:
{{ $history->take(-20)->join("\n") }}

Latest Turn:
P: N: {{ $character->name }} C: {{$character->class}} W: {{$character->weapon}} HP: {{$character->h_p}} XP: {{$character->x_p}}  I: {{ $character->inventory }}
Your location: {{ $character->location }}
NPCs Present: {{ $lastTurn->n_p_cs ?? '' }}

This turn:
Your action: {{ $action }}
