This is a character generator. Please generate a character with random name, stats, backstory and inventory for a character within a historic, realistic, medieval world.

A character with a high Level will have better stats, a character with a low Level will have poorer stats. This world contains a wide range of fantasy races, but most characters are human. When a human character is generated, please provide their ethnicity eg 'japanese', 'latino','asian','scandinavian','scottish','arabic','indian','dwarf','elf','fairy', etc but avoid racial stereotypes and cliches in their description and surroundings.

Use the following template:

Gender: Character's gender
Class: The character class (eg barbarian, bard, cleric, druid, fighter, monk, paladin, ranger, rogue, sorcerer, warlock, wizard, warlord, shaman,   swordmage, assassin, ninja, hunter, samurai, thief, swashbuckler, berserker, mage, archer, rogue, ronin, etc)
Name: The character's first and last name
Inventory: A set of 4-5 common and not so useful items, separated by a comma. At least one weapon with damage modifier (+2 damage). Rare enchanted items can give a boost to stats (+2 Magic)
Illustration description:  Describe in note form the character's class, appearance, race, gender. Include any unique identifying features. Emphasize important features using multiple brackets eg: '((Male dwarf thief)). Long ginger beard. Green Eyes. Amused expression. Worn leather armor. Carrying battleaxe. Mountains in background'
Level: The character's experience level
Stats: Strength x, Charisma x, Agility x, Magic x, Intelligence: x, Stealth x
Weapon: The character's weapon and damage modifier
Backstory: A paragraph or two explaining the life of this adventurer, their motivations and view on the world



Examples:

Gender: Female
Class: Paladin
Race: Fairy
Name: Aurora Brightstar
Illustration Description: ((Female Fairy Paladin)). Pale yellow skin and silvery-white hair. Pinkish butterfly wings. Wearing silver armor with a blue and silver tabard with the crest of a shining star. Holding a longsword and a round shield with a shining star. Confident expression. Soft magical glow surrounding her. Starry night sky in background.


Gender: Male
Class: Wizard
Race: Dwarf
Name: Egbert Ironfist
Illustration Description: ((Male Dwarf Wizard)). Long white beard and bushy moustache. Piercing blue eyes. Wearing a deep purple robe with gold trim and a pointed hat. Holding a long staff. Runes etched into his staff, some of them glowing. Mystical energy radiating from him. Mountains in the background.


Gender: Female
Class: Rogue
Race: Half-elf
Name: Emma Silverdawn
Illustration Description: ((Female Half-elf Rogue)). Long, light brown hair and hazel eyes. Dressed in dark clothing. Dagger in each hand. Running through a forest; leaves, branches and shadows around her. Mischievous smirk on her face.


Gender: Male
Class: Swordsman
Race: Human (European)
Name: John Ironheart
Illustration Description: European ((Male Swordsman)). Grey eyes, short brown hair and a neatly trimmed beard. Wearing an ornate silver breastplate and wielding a broadsword. Black cloak with fur trim. Standing atop a rocky mountain peak. Sun setting in the background. Intense gaze, determined face.


Gender: Female
Class: Bezerker
Race: Orc
Name: Basra Furyclaw
Illustration Description: ((Female Orc Bezerker)). Large, muscular frame and tusk-like teeth. Wearing chainmail armor, a red and black tabard, and a horned helmet. Holding a double-bladed axe in both hands. Eyes burning with rage. Battle raging in the background.


Avoid cliched and generic stereotypes in your descriptions. For example, not every Samurai needs cherry blossom, and not every Viking is a warrior.
This is set in an old world, medieval scene. No cities, just towns and villages. Appropriate clothing for the era eg no jeans. Clothing should also fit the character class, regardless of ethnicity. So a Female Indian warrior will wear armor, not a Sari. A viking magician will wear robes, not armor.
