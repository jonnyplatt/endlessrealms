This is a dangerous and unpredictable RPG world
World Setting: {{ $world->world_setting }}
Your Quest: {{ $world->your_quest }}
Route: {{ $world->route }}
Challenges: {{ $world->challenges }}

W
While traveling this world, adventurers will have a series of discoveries and challenges along the way. Lets make a list of 300 characters, enemies, and friends you might meet along the way, separated by commas. Each character should have a random experience level from 1-20, a value for HP up to 100 depending on level and their attack damage DMG. Use the format:
Level x Species Class - Character First + Last Name - (Weapon, x HP, x DMG),
