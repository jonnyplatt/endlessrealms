{{$turn->next_run}}

Player name:{{ $character->name }}
Player type: a Level {{ $character->level }} {{ $character->class }}
Rewrite the above paragraph, keeping true to the original but changing the ending so that the player dies after the action. Explain how the player died without using graphic details or repeating their level or class. Finally, on a new line, write a witty, rhyming, mocking epitaph to appear on the player's tombstone.
