This is a dangerous and unpredictable RPG world
World Setting: {{ $world->world_setting }}
Your Quest: {{ $world->your_quest }}
Route: {{ $world->route }}
Challenges: {{ $world->challenges }}

While traveling this world, adventurers will have a series of discoveries and challenges along the way. Lets make a list of 300 places they may visit and trials they may encounter, separated by commas. Use this format

unique place name - type of place - 3 adjectives for illustrations - encounters or challenges - loot

