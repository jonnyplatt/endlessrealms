This is a roguelike RPG generator with dungeons and dragons like mechanics. The game is hard and unforgiving, you should expect to die multiple times on your journey

The RPG theme is: {{ $world->theme }}

The story:
{{ $world->world_name }}:
{{ $world->world_setting  ?? 'You are an adventurer who needs to cross the mystical forest of Yendor, find the caves of Zorrow and descend through the underworld untill you meet the final boss, Yarg.' }}

You are seeking: {{ $world->goal_name ?? '' }} in: {{ $world->goal_destination ?? '' }} to: {{ $world->goal_reason ?? '' }}
To get there: {{ $world->route ?? '' }}.
{{ $world->challenges ?? '' }}

Game Rules
The game is set in an incredibly dangerous world where players must fight to survive. They earn XP by facing challenges and can gain stats when they reach 100 XP. Each turn involves a dice roll from 1-6 to determine the outcome of the player's actions. Low rolls are very bad outcomes, high rolls are only just successful. Negative buffs and high-level NPCs make the game difficult and unforgiving. Players should avoid repeating scenes.

In addition to the challenges and enemies they will face on their journey, the adventurers also face the risk of sudden death by misadventure. They may also encounter random events such as finding an item, chest, or loot along the way. Some of these items may be valuable, while others may turn out to be cursed or poisoned. The unpredictable nature of these random events adds an element of uncertainty and danger to the journey.

Each turn follows the template below. Please ensure you include every field on the template in your response.

Your action: Last action taken. If a value is unknown, use a hyphen (-)
Dice roll: 1-6 (1-2 is a disaster, 3-4 is a bad outcome, 5 is survival, 6 is ok)
Next Turn: A detailed paragraph of 4-5 sentences describing your location and any NPC present in rich detail. Who or what is here? What can the player do? What is likely to happen next? Most turns have negative, sometimes deadly outcomes.
Summary: Summarize the Next Turn paragraph in a sentence.
Illustration: A sentence describing the location + scene for our illustrator, with a list of descriptive adjectives, separated by commas
Location: Where you are now, the wider area you are in
HP Change: x
XP Change: x
Inventory Change: x
HP: x
XP: x
Level: x
Stats: Character stats after modifiers
Buffs: Active buffs, eg poisoned (-3-10 HP for x turns), cursed (-1-3 stats for x turns), wounded (-1-6 HP for x turns), burning (-6-10 HP for X turns), well fed (+3-5 HP for x turns) etc
Inventory: Items
Weapon: Weapon
Party: Party members after the action
NPCs: Who is here after the action. Remember NPCs are often hostile and will attack the player on sight
NPC Illustration: A sentence describing the NPC for our illustrator, with a list of descriptive adjectives, separated by commas, or none if there is no NPC
NPC Name: The name of the NPC, or none if there is no NPC
NPC Weapon: The name of the NPC's weapon, or none if they are unarmed
NPC Damage: The damage the NPC can do (eg -5-25 HP)
NPC HP: The NPC's current HP
NPC HP Change: The change in the NPC's HP
NPC Stats: The NPC's stats
NPC Level: The NPC's experience level
Actions: A list of 3 actions you could take next based on where you are and what you have just done, eg Attack Beast, Talk to Human, Run Away, Charm Stranger etc


Your Character:
Name: {{ $character->name }}, a {{ $character->class }}
Stats: {{ $character->stats }}
Level: {{ $character->level }}
HP: {{ $character->h_p ?? 100 }}
XP: {{ $character->x_p ?? 0 }}
Inventory: {{ $character->inventory }}
Your Weapon: {{ $character->weapon }}
Active Buffs: {{ $character->buffs ?? 'none' }}


Last turn
Your action: {{ $lastTurn->last_action ?? '' }}
Dice roll: {{ $lastTurn->dice_roll ?? '' }}
Next Turn: {{ $lastTurn->next_turn ?? '' }}
Summary: {{ $lastTurn->summary ?? '' }}
Illustration: {{ $lastTurn->illustration ?? '' }}
Location: {{$lastTurn->location ?? ''}}
HP Change: {{$lastTurn->hp_change ?? ''}}
XP Change: {{$lastTurn->xp_change ?? ''}}
Inventory Change: {{$lastTurn->inventory_change ?? ''}}
HP: {{$lastTurn->hp ?? ''}}
XP: {{$lastTurn->xp ?? ''}}
Level: {{$lastTurn->level ?? ''}}
Stats: {{$lastTurn->stats ?? ''}}
Buffs: {{$lastTurn->buffs ?? ''}}
Inventory: {{$lastTurn->inventory ?? ''}}
Weapon: {{$lastTurn->weapon ?? ''}}
Party: {{$lastTurn->party ?? ''}}
NPCs: {{$lastTurn->n_p_cs ?? ''}}
NPC Illustration: {{$lastTurn->n_p_c_illustration ?? ''}}
NPC Name: {{$lastTurn->n_p_c_name ?? ''}}
NPC Weapon: {{$lastTurn->n_p_c_weapon ?? ''}}
NPC HP: {{$lastTurn->n_p_c_hp ?? ''}}
NPC Stats: {{$lastTurn->n_p_c_stats ?? ''}}
NPC Level: {{$lastTurn->n_p_c_level ?? ''}}
NPC Damage: {{$lastTurn->n_p_c_damage ?? ''}}
Actions: {{$lastTurn->actions ?? ''}}

This turn
Your action: {{ $action }}

