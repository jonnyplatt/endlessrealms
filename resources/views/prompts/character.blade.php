This is a character generator. Please generate a character with random name, stats, backstory and inventory for a character within a {{ $world->theme }}.

A character with a high Level will have better stats, a character with a low Level will have poorer stats.
Each attribute has a number rating assigned to it. Normally they begin at 10, representing typical human ability, but can go as low as 1 for nearly useless, to 20 (or higher) for superhuman power. Anything in the 8 to 12 range is considered to be in the normal or average area for humans. Basic attribute scores of 6 or less are considered crippling—they are so far below the human norm that they are only used for severely handicapped characters. Scores of 15 or more are described as amazing—they are immediately apparent and draw constant comment. Characters with high scores for some attributes should have lower scores for others.

Use the following template:

Your Character:
Class: The character class (eg barbarian, bard, cleric, druid, fighter, monk, paladin, ranger, rogue, sorcerer, warlock, wizard, warlord, shaman, seeker, artificer, swordmage, assassin, ninja, hunter, samurai, thief, swashbuckler, berserker, mage, archer, rogue, ronin, etc)
Level: The experience level of the character
Name: The character's first and last name
Character: 2-3 sentence summary of character
Illustration: Describe the character's appearance
Stats: Strength x, Charisma x, Agility x, Magic x, Intelligence: x, Stealth x
Inventory: A set of 4-5 common and not so useful items, separated by a comma. At least one weapon with damage modifier (+x damage). Rare enchanted items can give a boost to stats (+x Magic)
Backstory: A paragraph or two explaining the life of this adventurer, their motivations and view on the world

Generated Character
Your Character:
