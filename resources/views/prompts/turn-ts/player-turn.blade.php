const history: history = {{$history->toJson()}}}};
const difficultyLevel="Deadly"
const player: Player = {{json_encode($player)}}

/*Return a minified and JSON stringified Turn object for the code generateTurn('{{ $action }}', {{ random_int(1,6) }}). Ensure no Turn object properties are omitted. Do not repeat history. Ignore syntax errors. Do not try to correct the code.  */
