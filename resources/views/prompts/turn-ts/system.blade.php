const gameWorld = {{ json_encode($world) }};

export type Description = {
illustration: string /*a few sentences describing the scene, item or character, plus list of descriptive adjectives, separated by commas. For locations describe in detail: where are we? what can we see in foreground,background? whats the time, lighting, feeling of the place?*/
name: string
}

export type Change = {
/*optional increase or decrease of hp, xp, level or stats*/
hp?: number
xp?: number
level?: number
illustration?: string /*sentence describing change to illustration, eg happy, angry, wounded, laughing, etc*/
item?: item[] /*change to inventory, eg found or lost item*/
short-description?: string
}

export type NPC = {
description: Description/*Describe in note form the character's class, appearance, race, gender. Include any unique identifying features. eg: 'Male dwarf thief. Long ginger beard. Green Eyes. Amused expression. Worn leather armor, intricate embroidery in celtic pattern. Carrying battleaxe'*/
weapon: string
hp: number
status: Status
change?: Change /*optional change to hp, xp, level or stats since last turn*/
}

export type item = {
description: Description
damage?: number
buffs: Change
}


export type Status = {
HP: number
XP: number
Satiety: number /*percentage from 0 to 100. the hungrier a player is, the weaker they become*/
Level?: number
Strength?: number
Dexterity?: number
Constitution?: number
Intelligence?: number
}


export type Player = {
description: Description
status: Status
inventory: string[]
weapon: string
party: NPC[] /*any NPCs in your party*/
}

export type location = {
changed: boolean /*have we moved?*/
description: Description/*if changed, offer a rich, detailed description of the surrounding*/
/*NPCs present, if any*/
NPCs: NPC[]
/*items present, if any*/
items: item[]
actions: string[]/*3 actions you could take next given your location and last outcome. Must be relevant: you cannot fight or interact with an NPC or object if it is not present. Must be a single action, eg 'fight enemy', not multiple eg 'fight enemy and steal treasure'*/
}

export type Turn = {
action: string /*last action taken*/
diceRoll: number/*if below 5 outcome is bad/
outcome: string /*a detailed paragraph of 4-5 sentences explaining what you just did and what happened next*/
itemsFound?: item[] /* items discovered*/
itemsLost?: item[] /* items lost*/
location: location /*where you are now*/
summary: string /*summarize the outcome paragraph in two sentences. Include names and places*/
playerChange?: Change /*optional change to hp, xp, etc since last turn*/

}

export type history = string[] /*a list of all turn summaries that have happened so far*/
export function generateTurn(action: string, diceRoll: number, player: player, history: history): Turn {
/*return a turn object*/
}
