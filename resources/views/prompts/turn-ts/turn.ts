/*
Each turn follows the template below. Please ensure you include every field on the template in your response.

Your action:Last action taken
Dice roll:X
Next Turn:A detailed paragraph of 4-5 sentences describing your location and any NPC present in rich detail. Who or what is here? What can the player do? What is likely to happen next? Most turns have negative, sometimes deadly outcomes.
Summary:Summarize the Next Turn paragraph in a sentence.
Illustration:A sentence describing the scene for our illustrator, with a list of descriptive adjectives, separated by commas
Location:Where you are now
HP Change:x
XP Change:x
Inventory Change:x
HP:x
XP:x
Level:x
Stats:Character stats
Buffs:Active buffs
Inventory:Items
Weapon:Weapon
Party:Party members after the action
NPCs:Who is here after the action
NPC Illustration:A sentence describing the NPC for our illustrator, with a list of descriptive adjectives, separated by commas, or none if there is no NPC
NPC Name:The name of the NPC, or none if there is no NPC
NPC Weapon:The name of the NPC's weapon, or none if they are unarmed or if there is no NPC
NPC HP:NPC HP, or none
NPC Stats:The NPC's stats, or none
NPC Level:The NPC's level, or none
NPC Damage:The NPC's damage, or none
Actions:A list of 3 actions you could take next based on where you are and what you have just done, eg Attack Beast, Talk to Human, Run Away, Charm Stranger etc

 */
// Using the above instructions, generate types for the template. Use short, concise properties. We should end up with the following types:
// type Player
// type NPC
// type Action
// type Turn

export type Description = {
    illustration: string /*sentence describing the scene, item or character, plus list of descriptive adjectives, separated by commas. For locations describe in detail: where are we? what can we see in foreground,background? whats the time, lighting, feeling of the place?*/
    name: string
}

export type Change = {
    /*optional increase or decrease of hp, xp, level or stats*/
    hp?: number
    xp?: number
    level?: number
    illustration?: string /*sentence describing change to illustration, eg happy, angry, wounded, laughing, etc*/
    item?: item[] /*change to inventory, eg found or lost item*/
}

export type NPC = {
    description: Description
    weapon: string
    hp: number
    status: Status
    change?: Change /*optional change to hp, xp, level or stats since last turn*/
}

export type item = {
    description: Description
}


export type Status = {
    HP: number
    XP: number
    Level?: number
    Strength?: number
    Dexterity?: number
    Constitution?: number
    Intelligence?: number
}

export type Player = {
    description: Description
    status: Status
    inventory: string[]
    weapon: string
    party: NPC[] /*any NPCs in your party*/
}

export type location = {
    changed: boolean /*have we moved?*/
    description: Description/*supply if changed*/
    /*NPCs present, if any*/
    NPCs: NPC[]
    /*items present, if any*/
    items: item[]
    actions: string[]/*3 actions you could take next given your location and last outcome. Must be relevant: you cannot fight or interact with an NPC or object if it is not present*/
}

export type Turn = {
    action: string /*last action taken*/
    diceRoll: number/*if below 5 outcome is bad/
    outcome: string /*a detailed paragraph of 4-5 sentences explaining what you just did and what happened next*/
    itemsFound?: item[] /* items discovered*/
    itemsLost?: item[] /* items lost*/
    location: location /*where you are now*/
    summary: string /*summarize the outcome paragraph in two sentences. Include names and places*/
    playerChange?: Change /*optional change to hp, xp, etc since last turn*/

}

export type history = string[] /*a list of all turn summaries that have happened so far*/
export function generateTurn(action: string, diceRoll: number, player: player, history: history): Turn {
    /*return a turn object*/
}

/*Execute the following code*/
const history: history = [];
const player: Player = {
    description: {illustration: 'dark handsome warrior with scar on left cheek', name: 'Bambista'},
    status: {HP: 100, XP: 0, Level: 1, Strength: 18, Dexterity: 7, Constitution: 14, Intelligence: 4},
    inventory: ['sword', 'shield', 'magical amulet'],
    weapon: 'sword',
    party: []
};
JSON.stringify(generateTurn('Continue your journey', rand_int(1,6), player, history))
/*minified output*/
