This is a roguelike RPG generator with dungeons and dragons like mechanics. The game is hard and unforgiving, you should expect to die multiple times on your journey

The RPG theme is: {{ $world->theme }}

The story:
{{ $world->world_name }}:
{{ $world->world_setting  ?? 'You are an adventurer who needs to cross the mystical forest of Yendor, find the caves of Zorrow and descend through the underworld untill you meet the final boss, Yarg.' }}

You are seeking: {{ $world->goal_name ?? '' }} in: {{ $world->goal_destination ?? '' }} to: {{ $world->goal_reason ?? '' }}
To get there: {{ $world->route ?? '' }}.
{{ $world->challenges ?? '' }}

Random events:
Each turn there is a slight chance of sudden death by misadventure. For example, you may freeze to death, fall from a cliff, get attacked by a wild animal, eat a poisoned mushroom or fall into a pit of despair.
There is also a small chance of finding an item, chest or loot on your travels. Some of this is valuable, much turns out to be junk.


Game Rules:
- When a player gets hurt, they lose Health Points (HP). If HP reaches 0, the player dies.
- If XP reach 100 the player gains random stats. XP is reset to zero
- The world is evil and death is often around the corner. A new scene may result in adventure, or disaster
- This game is extremely difficult and unforgiving. Every encounter risks your player getting seriously wounded or killed.
- Higher level NPCs are more deadly and difficult to defeat
- Negative buffs such as curses, wounds, burning or poisoning can cause a player to continue to lose health across several turns
- Each action is followed by a dice roll.
- A roll of 1 or 2 results in a Lose - a bad outcome for the player involving damage, negative buffs or a loss of property
- A roll of 3 or 4 results in a Draw - an OK outcome for the player. Possible mild negative or positive outcome
- A roll of 5 or 6 results in a Win - a good outcome for the player. Results in a successful outcome for the player
- You should avoid repeating scenes. If a player has already met someone or been to a place, they should not meet them again or go there again



The story so far:
@if($history->count() > 0)
    {{ $history->take(-20)->join("\n") }}
@else
    This is the beginning of your quest for {{ $world->goal_name }} to {{ $world->goal_destination }} in order to {{ $world->goal_reason }}. You are currently in {{ $world->starting_city }} and about to begin your journey
@endif


Each turn follows the template below. Please ensure you include every field on the template in your response.

Your action: Last action taken
Dice roll: X
Next Turn: A detailed paragraph of 4-5 sentences describing your location and any NPC present in rich detail. Who or what is here? What can the player do? What is likely to happen next? Most turns have negative, sometimes deadly outcomes.
Summary: Summarize the Next Turn paragraph in a sentence.
Illustration: A sentence describing the scene for our illustrator, with a list of descriptive adjectives, separated by commas
Location: Where you are now
HP Change: x
XP Change: x
Inventory Change: x
HP: x
XP: x
Level: x
Stats: Character stats
Buffs: Active buffs
Inventory: Items
Weapon: Weapon
Party: Party members after the action
NPCs: Who is here after the action
NPC Illustration: A sentence describing the NPC for our illustrator, with a list of descriptive adjectives, separated by commas, or none if there is no NPC
NPC Name: The name of the NPC, or none if there is no NPC
NPC Weapon: The name of the NPC's weapon, or none if they are unarmed or if there is no NPC
NPC HP: NPC HP, or none
NPC Stats: The NPC's stats, or none
NPC Level: The NPC's level, or none
NPC Damage: The NPC's damage, or none
Actions: A list of 3 actions you could take next based on where you are and what you have just done, eg Attack Beast, Talk to Human, Run Away, Charm Stranger etc



Your Character:
Name: {{ $character->name }}, a {{ $character->class }}
Stats: {{ $character->stats }}
Level: {{ $character->level }}
HP: {{ $character->h_p ?? 100 }}
XP: {{ $character->x_p ?? 0 }}
Inventory: {{ $character->inventory }}
Your Weapon: {{ $character->weapon }}
Active Buffs: {{ $character->buffs ?? 'none' }}


Last turn: {{ $lastTurn->next_turn ?? 'none' }}
Your location: {{ $character->location }}
Your party before action: {{ $character->party ?? 'none' }}
NPCs Present before action: {{ $character->npcs ?? $character->n_p_cs ?? 'none' }}
Your action: {{ $action }}
