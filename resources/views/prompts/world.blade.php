This is a RPG world generator. Using the template below, please imagine and describe a world to serve as the backdrop for our quest. Ensure each answer is given on a single line.

The theme is: {{ $world->theme }}

Template: Instructions with no line breaks
World name: World Name
Starting city: City Name
World Setting: A paragraph or two describing this world, its inhabitants, and the background for our story
Art style: A list of 5-6 adjectives describing this world for our illustrations of backdrops, separated by commas
Character Art Style: A list of 5-6 adjectives describing characters for portraits, separated by commas
Your quest: A paragraph or two explaining why must our adventurers travel and explore this world? What is their goal?
Goal name: The name of the goal
Goal destination: The place the goal is found
Goal reason: The reason for the goal (eg fame and fortune)
Route: A paragraph describing the key places they will pass on their journey with a short description for each.
Places: A comma separated list of place names
Challenges: A paragraph describing the enemies, creatures dangers and scenarios they will encounter, as well as any hazards along the way
Character Classes: The types of character that play this game, separated by a comma


World name:
